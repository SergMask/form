package com.netcracker.edu.backend.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "variant")
public class Variant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idvariant")
    private int id;

    @Column(name = "text")
    private String text;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "idquestion")
    @JsonBackReference
    private Question question;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Variant variant = (Variant) o;
        return id == variant.id &&
                question == variant.question &&
                Objects.equals(text, variant.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, question);
    }

    @Override
    public String toString() {
        return "Variant{" +
                "idVariant=" + id +
                ", text='" + text + '\'' +
                ", idQuestion=" + question +
                '}';
    }
}
