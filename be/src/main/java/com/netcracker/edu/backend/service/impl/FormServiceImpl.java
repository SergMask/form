package com.netcracker.edu.backend.service.impl;

import com.netcracker.edu.backend.entity.Form;
import com.netcracker.edu.backend.entity.ThemeTag;
import com.netcracker.edu.backend.repository.FormRepository;
import com.netcracker.edu.backend.repository.ThemeTagRepository;
import com.netcracker.edu.backend.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FormServiceImpl implements FormService {

    @Autowired
    private FormRepository formRepository;
    @Autowired
    private ThemeTagRepository themeTagRepository;

    @Override
    public List<Form> getAllPrivateForms(int page) {
        Pageable pageable = new PageRequest(page, 8, Sort.by("id").descending());
        Page<Form> formPage = formRepository.getAllByIsPrivate(pageable, true);
        return formPage.getContent();
    }

    @Override
    public List<Form> getAllPublicForms(int page) {
        Pageable pageable = new PageRequest(page, 8, Sort.by("id").descending());
        Page<Form> formPage = formRepository.getAllByIsPrivate(pageable, false);
        return formPage.getContent();
    }

    @Override
    public Form getFormById(Integer id) {
        return formRepository.findById(id).get();
    }

    @Override
    public List<Form> getFormsByUserId(int id) {
        return formRepository.getFormsByUserId(id);
    }

    @Override
    public void deleteForm(Integer id) {
        formRepository.deleteById(id);
    }

    @Override
    public Form saveForm(Form form) {
        form.getThemeTags().forEach(tag -> {
            ThemeTag resultTag = themeTagRepository.getThemeTagByName(tag.getName());
            if (resultTag == null) {
                resultTag = themeTagRepository.save(tag);
                tag = resultTag;
            } else {
                tag.setId(resultTag.getId());
            }
        });
        return formRepository.save(form);
    }

    @Override
    public int getPageCount(Boolean isPrivate) {
        List<Form> forms = formRepository.getAllByIsPrivate(isPrivate);

        return (int) Math.ceil((double) forms.size() / 8);
    }


}
