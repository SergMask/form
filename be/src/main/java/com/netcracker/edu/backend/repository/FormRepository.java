package com.netcracker.edu.backend.repository;

import com.netcracker.edu.backend.entity.Form;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface FormRepository extends JpaRepository<Form, Integer> {
    Page<Form> getAllByIsPrivate(Pageable pageable, Boolean isPrivate);

    @Query(value = "select f from Form f where f.user=(select u from User u where u.id=?1)")
    List<Form> getFormsByUserId(int id);

    List<Form> getAllByIsPrivate(Boolean isPrivate);


}
