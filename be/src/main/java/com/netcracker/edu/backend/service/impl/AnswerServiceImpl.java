package com.netcracker.edu.backend.service.impl;

import com.netcracker.edu.backend.entity.Answer;
import com.netcracker.edu.backend.entity.Question;
import com.netcracker.edu.backend.entity.Statistics;
import com.netcracker.edu.backend.entity.VarQuestionStatistics;
import com.netcracker.edu.backend.repository.AnswerRepository;
import com.netcracker.edu.backend.repository.QuestionRepository;
import com.netcracker.edu.backend.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;


    @Override
    public List<Answer> getAllVariantAnswersByQuestionId(int id) {
        return answerRepository.getAllVariantAnswersByQuestionId(id);
    }

    @Override
    public List<Answer> getAllTextAnswersByQuestionId(int id) {
        if (answerRepository.getAllTextAnswersByQuestionId(id).equals(Collections.emptyList())) {
            return Collections.emptyList();
        }
        return answerRepository.getAllTextAnswersByQuestionId(id);
    }

    @Override
    public List<Answer> saveAnswers(List<Answer> answers) {
        return answerRepository.saveAll(answers);
    }

    @Override
    public List<Statistics> getStatisticOfVariantQuestion(int id) {
        List<Answer> answers = getAllVariantAnswersByQuestionId(id);
        if (answers.equals(Collections.emptyList())) {
            return Collections.emptyList();
        }
        int capacity = answers.get(0).getQuestion().getVariants().size();
        List<Statistics> statistics = new ArrayList<>();

        for (int i = 0; i < capacity; i++) {
            statistics.add(new Statistics());
        }
        for (int i = 0; i < capacity; i++) {
            statistics.get(i).setVarName(answers.get(0).getQuestion().getVariants().get(i).getText());
            statistics.get(i).setId(answers.get(0).getQuestion().getVariants().get(i).getId());
            statistics.get(i).setQuestionId(answers.get(0).getQuestion().getId());
        }

        for (Statistics statistic : statistics) {
            double count = 0;
            for (Answer answer : answers) {
                if (statistic.getId() == answer.getIdVariant()) {
                    count++;
                }
            }
            statistic.setAnswerQuantity((int) count);
            statistic.setPercents((count / answers.size()) * 100);
        }
        return statistics;
    }

    @Override
    public ArrayList<VarQuestionStatistics> getStatisticOfAllVariantQuestionByFormId(int id) {
        List<Question> questions = questionRepository.getAllByForm_Id(id);
        ArrayList<VarQuestionStatistics> statistics = new ArrayList<>();
        for (Question question : questions) {
            if (question.getType().equals("var")) {
                VarQuestionStatistics stat = new VarQuestionStatistics();
                stat.setId(question.getId());
                stat.setStatistics(getStatisticOfVariantQuestion(question.getId()));
                statistics.add(stat);
            }
        }
        return statistics;
    }
}
