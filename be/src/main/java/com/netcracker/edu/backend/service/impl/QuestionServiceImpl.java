package com.netcracker.edu.backend.service.impl;

import com.netcracker.edu.backend.entity.Question;
import com.netcracker.edu.backend.repository.QuestionRepository;
import com.netcracker.edu.backend.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public List<Question> getAllQuestionsByFormId(int id) {
        return questionRepository.getAllByForm_Id(id);
    }
}
