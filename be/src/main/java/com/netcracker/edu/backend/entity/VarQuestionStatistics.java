package com.netcracker.edu.backend.entity;

import java.util.List;

public class VarQuestionStatistics {
    private int id;
    private List<Statistics> statistics;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Statistics> getStatistics() {
        return statistics;
    }

    public void setStatistics(List<Statistics> statistics) {
        this.statistics = statistics;
    }
}
