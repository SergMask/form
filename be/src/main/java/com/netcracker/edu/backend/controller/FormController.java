package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.entity.Form;
import com.netcracker.edu.backend.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/forms")
public class FormController {

    @Autowired
    private FormService formService;

    @RequestMapping(value = "/private", method = RequestMethod.GET)
    public List<Form> getAllPrivateForms(@RequestParam int page) {
        return formService.getAllPrivateForms(page);
    }

    @RequestMapping(value = "/public", method = RequestMethod.GET)
    public List<Form> getAllPublicForms(@RequestParam int page) {
        return formService.getAllPublicForms(page);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Form> getFormById(@PathVariable(name = "id") String id) {
        Form form = formService.getFormById(Integer.valueOf(id));
        return ResponseEntity.ok(form);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public List<Form> getFormsByUserId(@PathVariable int id) {
        return formService.getFormsByUserId(id);
    }


    @RequestMapping(method = RequestMethod.POST)
    public Form saveForm(@RequestBody Form form) {
        return formService.saveForm(form);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteForm(@PathVariable(name = "id") Integer id) {
        formService.deleteForm(id);
    }

    @RequestMapping(value = "/pagecount/public", method = RequestMethod.GET)
    public int pageCountPublic() {
        return formService.getPageCount(false);
    }

    @RequestMapping(value = "/pagecount/private", method = RequestMethod.GET)
    public int pageCountPrivate() {
        return formService.getPageCount(true);
    }


}
