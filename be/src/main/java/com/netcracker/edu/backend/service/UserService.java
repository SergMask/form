package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.entity.User;

import java.util.List;

public interface UserService {
    User getUserByEmail(String email);
    User save(User user);



}
