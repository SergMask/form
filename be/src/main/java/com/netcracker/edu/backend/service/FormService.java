package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.entity.Form;

import java.util.List;

public interface FormService {
    List<Form> getAllPrivateForms(int page);

    List<Form> getAllPublicForms(int page);

    Form getFormById(Integer id);

    void deleteForm(Integer id);

    Form saveForm(Form form);

    List<Form> getFormsByUserId(int id);

    int getPageCount(Boolean isPrivate);

}
