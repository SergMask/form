package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.entity.Question;

import java.util.List;

public interface QuestionService {
    List<Question> getAllQuestionsByFormId(int id);

}
