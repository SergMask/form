package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.entity.Answer;
import com.netcracker.edu.backend.entity.Statistics;
import com.netcracker.edu.backend.entity.VarQuestionStatistics;

import java.util.ArrayList;
import java.util.List;

public interface AnswerService {
    List<Answer> saveAnswers(List<Answer> answers);

    List<Answer> getAllVariantAnswersByQuestionId(int id);

    List<Answer> getAllTextAnswersByQuestionId(int id);

    List<Statistics> getStatisticOfVariantQuestion(int id);

    ArrayList<VarQuestionStatistics> getStatisticOfAllVariantQuestionByFormId(int id);
}
