package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.entity.Answer;
import com.netcracker.edu.backend.entity.VarQuestionStatistics;
import com.netcracker.edu.backend.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/answers")
public class AnswerController {
    @Autowired
    private AnswerService answerService;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public List<Answer> saveAnswers(@RequestBody List<Answer> answers) {
        return answerService.saveAnswers(answers);
    }

    @RequestMapping(value = "/text/{id}", method = RequestMethod.GET)
    public List<Answer> getAllTextAnswersByQuestionId(@PathVariable int id) {
        return answerService.getAllTextAnswersByQuestionId(id);
    }

    @RequestMapping(value = "/form/{id}/statistics", method = RequestMethod.GET)
    public ArrayList<VarQuestionStatistics> getStatisticOfAllVariantQuestionByFormId(@PathVariable int id) {
        return answerService.getStatisticOfAllVariantQuestionByFormId(id);
    }
}
