package com.netcracker.edu.backend.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "question")
public class Question {
    @Id
    @Column(name = "idquestion")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "mandatory")
    private String mandatory;
    @Column(name = "type")
    private String type;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "idform")
    @JsonBackReference
    private Form form;

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "question")
    @JsonManagedReference
    private List<Variant> variants;


    public List<Variant> getVariants() {
        return variants;
    }

    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }


    public Question(String name, String mandatory, String type, Form form, List<Variant> variants) {
        this.name = name;
        this.mandatory = mandatory;
        this.type = type;
        this.form = form;
        this.variants = variants;
    }

    public Question() {

    }
}
