package com.netcracker.edu.backend.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "form")
public class Form {

    @Id
    @Column(name = "idform")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "is_private")
    private Boolean isPrivate;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "iduser")
    private User user;
    @OneToMany(cascade = {CascadeType.ALL})
    @JsonManagedReference
    private List<Question> questions;

    @ManyToMany()
    @JoinTable(name = "form_tag", joinColumns = {@JoinColumn(name = "idform")}, inverseJoinColumns = {@JoinColumn(name = "idthemetag")})
    private Set<ThemeTag> themeTags;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Question> getQuestions() {
        return questions;
    }


    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Set<ThemeTag> getThemeTags() {
        return themeTags;
    }

    public void setThemeTags(Set<ThemeTag> themeTags) {
        this.themeTags = themeTags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Form form = (Form) o;
        return id == form.id &&
                user == form.user &&
                Objects.equals(isPrivate, form.isPrivate) &&
                Objects.equals(name, form.name);
    }


    public Form(Boolean isPrivate, String name, User user, List<Question> questions, Set<ThemeTag> themeTags) {
        this.isPrivate = isPrivate;
        this.name = name;
        this.user = user;
        this.questions = questions;
        this.themeTags = themeTags;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isPrivate, name, user);
    }


    public Form() {

    }
}
