package com.netcracker.edu.backend.repository;

import com.netcracker.edu.backend.entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AnswerRepository extends JpaRepository<Answer, Integer> {
    @Override
    <S extends Answer> List<S> saveAll(Iterable<S> iterable);

    @Query(value = "select a from Answer a where (idVariant is not null and question.id=?1)")
    List<Answer> getAllVariantAnswersByQuestionId(int id);

    @Query(value = "select a from Answer a where (answer is not null and question.id=?1)")
    List<Answer> getAllTextAnswersByQuestionId(int id);
}
