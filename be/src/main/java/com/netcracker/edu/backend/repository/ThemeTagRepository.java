package com.netcracker.edu.backend.repository;

import com.netcracker.edu.backend.entity.ThemeTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ThemeTagRepository extends JpaRepository<ThemeTag, Integer> {

    ThemeTag getThemeTagByName(String name);
}
