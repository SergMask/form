import {Component, Input, OnInit} from '@angular/core';
import {QuestionModel} from "../../../../models/question.model";
import {AnswerModel} from "../../../../models/answer.model";

@Component({
  selector: 'app-text-answer',
  templateUrl: './text-answer.component.html',
  styleUrls: ['./text-answer.component.css']
})
export class TextAnswerComponent implements OnInit {
  @Input()
  answer: AnswerModel;
  @Input()
  textAnswerId: string;
  @Input()
  question: QuestionModel;


  constructor() {

  }


  ngOnInit() {
    this.answer.question.id = this.question.id;
  }

}
