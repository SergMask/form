import {Component, Input, OnInit} from '@angular/core';
import {QuestionModel} from "../../../../models/question.model";
import {AnswerModel} from "../../../../models/answer.model";

@Component({
  selector: 'app-var-answer',
  templateUrl: './var-answer.component.html',
  styleUrls: ['./var-answer.component.css']
})
export class VarAnswerComponent implements OnInit {


  @Input()
  answer: AnswerModel;
  @Input()
  question: QuestionModel;

  constructor() {
  }

  ngOnInit() {
    this.answer.question.id = this.question.id;
  }

  onChange(id: number): void {
    this.answer.idVariant = id;
  }

}
