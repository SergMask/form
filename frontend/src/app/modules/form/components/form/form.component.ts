import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Input()
  id: string;

  @Input()
  username: string;

  @Input()
  name: string;

  @Input()
  linkToAns: string;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  public navigateToAnswer(): void {
    console.log(this.id);
    this.router.navigateByUrl("/answer/" + this.id)

  }

}
