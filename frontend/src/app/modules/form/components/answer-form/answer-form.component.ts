import {Component, Input, OnInit} from '@angular/core';
import {FormModel} from "../../../../models/form.model";
import {Constants} from "../../../../constants/constants";
import {AnswerModel} from "../../../../models/answer.model";

@Component({
  selector: 'app-answer-form',
  templateUrl: './answer-form.component.html',
  styleUrls: ['./answer-form.component.css']
})
export class AnswerFormComponent implements OnInit {
  @Input()
  form: FormModel;

  @Input()
  answers: AnswerModel[];

  variantType: string = Constants.VARIANT_TYPE_QUESTION;
  textType: string = Constants.TEXT_TYPE_QUESTION;

  ngOnInit() {
    console.log(this.answers);
  }

}
