import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{FormComponent} from "./components/form/form.component";
import {SharedModule} from "../../shared/shared.module";
import { VarAnswerComponent } from './components/var-answer/var-answer.component';
import { TextAnswerComponent } from './components/text-answer/text-answer.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";




@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    VarAnswerComponent,
    FormComponent,
    TextAnswerComponent
  ],
  declarations: [FormComponent, VarAnswerComponent, TextAnswerComponent]
})
export class FormModule { }
