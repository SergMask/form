import {Component, OnInit} from '@angular/core';
import {LoginModel} from "../../../../models/login.model";
import {StorageService} from "../../../../services/storage.service";
import {AuthToken, UserService} from "../../../../services/user.service";
import {UserModel} from "../../../../models/user.model";
import {Router} from "@angular/router";

import {FormControl, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'app-register-button',
  templateUrl: './register-button.component.html',
  styleUrls: ['./register-button.component.css']
})
export class RegisterButtonComponent implements OnInit {

  public loginModel: LoginModel = {};
  public showCheckYourSetDataAlert: boolean = false;

  loginForm: FormGroup = new FormGroup({
    "email": new FormControl("", [Validators.required, Validators.email, Validators.maxLength(100)]),
    "password": new FormControl("", [Validators.required, Validators.maxLength(200)])
  });

  constructor(private storageService: StorageService,
              private userService: UserService,
              private router: Router) {

  }

  ngOnInit() {
  }

  public onSubmit(): void {
    this.userService.generateToken(this.loginModel)
      .subscribe((authToken: AuthToken) => {
        if (authToken.token) {
          this.storageService.setToken(authToken.token);
          this.userService.getAuthorizedUser()
            .subscribe((userModel: UserModel) => {
              this.storageService.setCurrentUser(userModel);
            });
        }
      }, (error) => {
        if (error.status === 401) {
          this.showCheckYourSetDataAlert = true;
        } else {
          alert(error.message);
        }
      });

  }

  public navigateToRegisterPage(): void {
    this.router.navigateByUrl("/register-page")

  }

}
