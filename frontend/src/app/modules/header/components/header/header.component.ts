import {Component, OnInit} from '@angular/core';
import {LoginModel} from "../../../../models/login.model";
import {StorageService} from "../../../../services/storage.service";
import {UserService} from "../../../../services/user.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  constructor(private storageService: StorageService) {
  }

  ngOnInit() {
  }

  public logout(): void {
    this.storageService.clearToken();
    this.storageService.setCurrentUser(null);
  }

}
