import { NgModule } from "@angular/core";
import {HeaderComponent} from "./components/header/header.component";
import { RegisterButtonComponent } from './components/register-button/register-button.component';
import {SharedModule} from "../../shared/shared.module";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    HeaderComponent,
    RegisterButtonComponent,
      ],
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [],
  exports: [HeaderComponent]
})
export class HeaderModule {}
