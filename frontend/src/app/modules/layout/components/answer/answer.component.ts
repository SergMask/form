import {Component, OnInit} from '@angular/core';
import {FormService} from "../../../../services/form.service";
import {FormModel} from "../../../../models/form.model";
import {ActivatedRoute, Router} from "@angular/router";
import {AnswerModel} from "../../../../models/answer.model";
import {AnswerService} from "../../../../services/answer.service";
import {QuestionModel} from "../../../../models/question.model";

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {

  form: FormModel;

  answers: AnswerModel[] = [];

  constructor(private formService: FormService,
              private serviceRouter: ActivatedRoute,
              private answerService: AnswerService,
              private router: Router) {

  }


  ngOnInit() {
    const id = this.serviceRouter.snapshot.paramMap.get("id");
    this.formService.getFormById(id).subscribe(form => {
      for (let i of form.questions) {
        const answer: AnswerModel = new AnswerModel();
        answer.question = new QuestionModel();
        this.answers.push(answer);

      }
      this.form = form;
    });
  }

  public saveAnswers(): void {
    this.answerService.saveAnswers(this.answers).subscribe(() => {
      this.navigateToHome()
    });
  }

  public navigateToHome(): void {
    this.router.navigateByUrl("/answer-buffer-page");

  }


}
