import {Component, OnInit} from '@angular/core';
import {AuthToken, UserService} from "../../../../services/user.service";
import {UserModel} from "../../../../models/user.model";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {StorageService} from "../../../../services/storage.service";
import {LoginModel} from "../../../../models/login.model";

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {

  registerForm: FormGroup = new FormGroup({
    "name": new FormControl("", Validators.required),
    "email": new FormControl("", [Validators.required, Validators.email]),
    "password": new FormControl("", Validators.required),
  });

  userModel: UserModel = new UserModel();
  public loginModel: LoginModel = {};

  constructor(private userService: UserService,
              private storageService: StorageService,
              private router: Router) {
  }

  ngOnInit() {

  }

  public saveUser(user: UserModel): void {
    this.userService.saveUser(user).subscribe(() => {
      this.convert();
      this.onSubmit();
      this.navigateToHome();
    }, () => {
    });
  }


  public navigateToHome(): void {
    this.router.navigateByUrl("/home");
  }

  public onSubmit(): void {
    this.userService.generateToken(this.loginModel)
      .subscribe((authToken: AuthToken) => {
        if (authToken.token) {
          this.storageService.setToken(authToken.token);
          this.userService.getAuthorizedUser()
            .subscribe((userModel: UserModel) => {
              this.storageService.setCurrentUser(userModel);
            });
        }
      });
  }

  public convert(): void {
    this.loginModel.email = this.userModel.email;
    this.loginModel.password = this.userModel.password;
  }
}
