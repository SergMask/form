import {Component, OnDestroy, OnInit} from "@angular/core";

import {FormService} from "../../../../services/form.service";
import {Subscription} from "rxjs";
import {Form} from "../../../../models/form.model.home.view";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit, OnDestroy {

  public forms: Form[];
  public page: number = 1;
  public pageQuantity: number;

  subscriptionsArray: Subscription[] = [];

  constructor(private formService: FormService) {
  }

  ngOnInit() {
    this.subscriptionsArray.push(this.formService.getAllForms(0).subscribe((forms: Form[]) => {
      this.forms = forms;
      console.log(forms);
    }));
    this.subscriptionsArray.push(this.formService.getPageCountPublic().subscribe((count: number) => {
      this.pageQuantity = count;
      console.log(this.pageQuantity);
    }))
  }

  nextPage(): void {
    this.subscriptionsArray.push(this.formService.getAllForms(this.page).subscribe((forms: Form[]) => {
      for (let item of forms) {
        this.forms.push(item);
      }
    }));
    this.page = this.page + 1;
  }

  ngOnDestroy(): void {
    this.subscriptionsArray.forEach(subscription => subscription.unsubscribe());
  }
}

