import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";
import {QuestionModel} from "../../../../models/question.model";
import {QuestionService} from "../../../../services/question.service";
import {AnswerService} from "../../../../services/answer.service";
import {FormService} from "../../../../services/form.service";
import {FormStatistics} from "../../../../models/FormStatistics";
import {VarQuestionStatistics} from "../../../../models/VarQuestionStatistics";
import {Constants} from "../../../../constants/constants";
import {AnswerModel} from "../../../../models/answer.model";

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit, OnDestroy {
  questions: QuestionModel[];
  subscriptionsArray: Subscription[] = [];
  statistics: VarQuestionStatistics[];
  textAnswers: AnswerModel[];
  form: FormStatistics = new FormStatistics();
  variantType: string = Constants.VARIANT_TYPE_QUESTION;
  textType: string = Constants.TEXT_TYPE_QUESTION;


  constructor(private serviceRouter: ActivatedRoute,
              private questionService: QuestionService,
              private answerService: AnswerService,
              private formService: FormService) {
  }


  ngOnInit() {
    const id = this.serviceRouter.snapshot.paramMap.get("id");
    this.subscriptionsArray.push(this.questionService.getAllQuestionsByFormId(id).subscribe((questions: QuestionModel[]) => {
      this.questions = questions;
    }));
    this.subscriptionsArray.push(this.answerService.getStatisticOfAllVariantQuestionByFormId(id).subscribe((statistics: VarQuestionStatistics[]) => {
      this.statistics = statistics;
    }));
    this.subscriptionsArray.push(this.formService.getStatisticFormById(id).subscribe((formStatistics: FormStatistics) => {
      this.form = formStatistics;
    }))
  }

  ngOnDestroy(): void {
    this.subscriptionsArray.forEach(subscription => subscription.unsubscribe());
  }


  public showTextQuestions(id: string): void {
    this.subscriptionsArray.push(this.answerService.getAllTextAnswersByQuestionId(id).subscribe((textAnswers: AnswerModel[]) => {
      this.textAnswers = textAnswers;
    }))
  }
}
