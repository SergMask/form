import {Component, OnDestroy, OnInit} from '@angular/core';
import {StorageService} from "../../../../services/storage.service";
import {FormService} from "../../../../services/form.service";
import {Form} from "../../../../models/form.model.home.view";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-private-forms',
  templateUrl: './private-forms.component.html',
  styleUrls: ['./private-forms.component.css']
})
export class PrivateFormsComponent implements OnInit, OnDestroy {

  public forms: Form[];
  public page:number=1;
  public pageQuantity:number;

  subscriptionsArray: Subscription[] = [];

  constructor(private storageService:StorageService,
              private formService:FormService) { }

  ngOnInit() {
    this.subscriptionsArray.push(this.formService.getAllPrivateForms(0).subscribe((forms: Form[]) => {
      this.forms = forms;
      console.log(forms);
    }));
    this.subscriptionsArray.push(this.formService.getPageCountPrivate().subscribe((count:number)=>{
      this.pageQuantity=count;
      console.log(this.pageQuantity);
    }))
  }

  nextPage():void{
    this.subscriptionsArray.push(this.formService.getAllPrivateForms(this.page).subscribe((forms:Form[])=>{
      for (let item of forms){
        this.forms.push(item);
      }
    }));
    this.page=this.page+1;
  }

  ngOnDestroy(): void {
    this.subscriptionsArray.forEach(subscription => subscription.unsubscribe())
  }

}
