import {Component, OnInit} from '@angular/core';
import {StorageService} from "../../../../services/storage.service";
import {FormService} from "../../../../services/form.service";
import {Subscription} from "rxjs";
import {FormStatistics} from "../../../../models/FormStatistics";
import {Router} from "@angular/router";

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {
  forms: FormStatistics[];
  subscriptionsArray: Subscription[] = [];

  constructor(private storageService: StorageService,
              private formService: FormService,
              private router: Router) {
  }

  ngOnInit() {
    this.subscriptionsArray.push(this.formService.getFormsByUserId(this.storageService.getCurrentUser().id).subscribe((forms: FormStatistics[]) => {
      this.forms = forms;
      console.log(forms);
    }));
  }


  public findWithAttr(array, attr, value) {
    for (var i = 0; i < array.length; i += 1) {
      if (array[i][attr] === value) {
        return i;
      }
    }
    return -1;
  }


  public deleteForm(id: number): void {
    this.forms.splice(this.findWithAttr(this.forms, "id", id), 1);
    this.formService.deleteForm(id).subscribe();
  }

  public navigateToStatisticPage(id: number): void {
    this.router.navigateByUrl("/statistic/" + id)

  }


}
