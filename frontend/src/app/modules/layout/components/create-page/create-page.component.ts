import {Component, OnDestroy, OnInit} from '@angular/core';
import {Constants} from "../../../../constants/constants";
import {FormService} from "../../../../services/form.service";
import {FormModel} from "../../../../models/form.model";
import {QuestionModel} from "../../../../models/question.model";
import {VariantModel} from "../../../../models/variant.model";
import {Router} from "@angular/router";
import {StorageService} from "../../../../services/storage.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ThemeTagModel} from "../../../../models/themeTag.model";


@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.css']
})
export class CreatePageComponent implements OnInit, OnDestroy {
  public formModel: FormModel = new FormModel();
  public textType: string = Constants.TEXT_TYPE_QUESTION;
  public variantType: string = Constants.VARIANT_TYPE_QUESTION;
  public themeTagName: string;
  validationForm: FormGroup = new FormGroup({
    "formName": new FormControl("", [Validators.required, Validators.maxLength(100)]),
    "themeTagName": new FormControl("", [Validators.maxLength(45)])
  });


  constructor(private formService: FormService,
              private router: Router,
              private storageService: StorageService) {

    this.formModel.private = false;
    this.formModel.themeTags = [];
    this.formModel.questions = [];


  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }


  public pushNewQuestionValidator(name: string): void {
    this.validationForm.addControl(name, new FormControl("", [Validators.required, Validators.maxLength(300)]));
  }

  public pushNewVariantValidator(name: string): void {
    this.validationForm.addControl(name, new FormControl("", [Validators.required, Validators.maxLength(100)]));
  }

  public removeValidator(name: string): void {
    this.validationForm.removeControl(name);
  }

  public addTextQuestion(): void {
    if (!this.formModel.questions) {
      this.formModel.questions = [];
    }
    const question = ({
      type: Constants.TEXT_TYPE_QUESTION, variants: []
      , mandatory: "0", id: this.formModel.questions.length.toString()
    } as QuestionModel);
    this.formModel.questions.push(question);
    this.pushNewQuestionValidator("question" + question.id);
  }

  public addVariantQuestion(): void {
    if (!this.formModel.questions) {
      this.formModel.questions = [];
    }
    const question = ({
      type: Constants.VARIANT_TYPE_QUESTION, variants: [
        {id: "1"} as VariantModel,
        {id: "2"} as VariantModel
      ], mandatory: "0", id: this.formModel.questions.length.toString()
    } as QuestionModel);
    this.formModel.questions.push(question);
    this.pushNewQuestionValidator("question" + question.id);
    this.pushNewVariantValidator("variant" + question.id + "_" + "1");
    this.pushNewVariantValidator("variant" + question.id + "_" + "2");
  }


  public removeQuestions(i: QuestionModel): void {
    if (this.formModel.questions[this.formModel.questions.indexOf(i)].type == this.textType) {
      this.removeValidator("question" + i.id)
    }
    if (this.formModel.questions[this.formModel.questions.indexOf(i)].type == this.variantType) {
      for (let variant of this.formModel.questions[this.formModel.questions.indexOf(i)].variants) {
        this.removeValidator("variant" + i.id + "_" + variant.id)
      }
    }
    this.formModel.questions.splice(this.formModel.questions.indexOf(i), 1);


  }

  public saveForm(): void {
    this.formModel.user = this.storageService.getCurrentUser();
    for (let q of this.formModel.questions) {
      if (q.type == this.textType) {
        q.id = null;
      }
      if (q.type == this.variantType) {
        for (let v of q.variants) {
          v.id = null;
        }
        q.id = null;
      }
    }
    this.formService.saveForm(this.formModel).subscribe(() => {
      this.navigateToHome();
    });
  }

  public navigateToHome(): void {
    this.router.navigateByUrl("/buffer-page");
  }

  public addVariant(i: number): void {
    const variant = ({
      id: (this.formModel.questions[i].variants.length + 1).toString()
    } as VariantModel);
    this.formModel.questions[i].variants.push(variant);
    this.pushNewVariantValidator("variant" + (i).toString() + "_" + +variant.id.toString());
  }

  public addThemeTag(): void {
    const tag = ({
      name: this.themeTagName
    } as ThemeTagModel);
    this.formModel.themeTags.push(tag);
    this.themeTagName = null;
  }

}
