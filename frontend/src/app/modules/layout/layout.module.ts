import { NgModule } from "@angular/core";
import {NotFoundComponent} from "./components/404/not-found.component";
import {HomeComponent} from "./components/home/home.component";
import {HeaderModule} from "../header/header.module";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {FormModule} from "../form/form.module";
import { AnswerComponent } from './components/answer/answer.component';
import {AnswerFormComponent} from "../form/components/answer-form/answer-form.component";
import { ProfilePageComponent } from './components/profile-page/profile-page.component';
import { CreatePageComponent } from './components/create-page/create-page.component';
import {ButtonsModule} from "ngx-bootstrap";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { BufferPageComponent} from "./components/buffer-page/buffer-page.component";
import { RegisterPageComponent } from './components/register-page/register-page.component';
import { PrivateFormsComponent } from './components/private-forms/private-forms.component';
import { AnswerBufferPageComponent } from './components/answer-buffer-page/answer-buffer-page.component';
import { StatisticsComponent } from './components/statistics/statistics.component';


@NgModule({
  declarations: [
    HomeComponent,
    NotFoundComponent,
    AnswerFormComponent,
    AnswerComponent,
    AnswerFormComponent,
    ProfilePageComponent,
    CreatePageComponent,
    BufferPageComponent,
    RegisterPageComponent,
    PrivateFormsComponent,
    AnswerBufferPageComponent,
    StatisticsComponent
  ],
  imports: [
    HeaderModule,
    SharedModule,
    RouterModule,
    FormModule,
    ButtonsModule.forRoot(),
    CommonModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [],
  exports: [HomeComponent, NotFoundComponent,ProfilePageComponent]
})
export class LayoutModule {}
