import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Input()
  text: string;
  @Input()
  button_type: string = "";
  @Input()
  class: string = "";

  @Input()
  link: string;
  @Input()
  toggle:string;
  @Input()
  disabled:boolean;

  @Input()
  target:string;
  @Output()
  clickChange: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

}
