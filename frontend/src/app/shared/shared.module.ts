import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";

import {ButtonComponent} from "./components/button/button.component";
import {BrowserModule} from "@angular/platform-browser";
import {CommonModule} from "@angular/common";
import {BsDropdownModule, ModalModule, TooltipModule} from "ngx-bootstrap";

import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [ButtonComponent],
  imports: [
    BrowserModule,
    CommonModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
  ],
  schemas: [NO_ERRORS_SCHEMA],
  exports: [ButtonComponent]
})
export class SharedModule {

}
