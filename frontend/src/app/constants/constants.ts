export class Constants {
  public static TEXT_TYPE_QUESTION: string = "text";
  public static VARIANT_TYPE_QUESTION: string = "var";
  public static HTTP_URL: string = "http://localhost:8081/api/forms";
}
