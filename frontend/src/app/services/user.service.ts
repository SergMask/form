import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {LoginModel} from "../models/login.model";
import {UserModel} from "../models/user.model";

@Injectable({
  providedIn: "root"
})

export class UserService {

  private httpUrl: string = "http://localhost:8081";

  constructor(private http: HttpClient) {
  }

  public generateToken(login: LoginModel): Observable<AuthToken> {
    return this.http.post<AuthToken>(this.httpUrl+"/token/generate-token", login);
  }

  public getAuthorizedUser(): Observable<UserModel> {
    return this.http.get<UserModel>(this.httpUrl+"/api/users/current");
  }
  public saveUser(user:UserModel):Observable<any>{
    return this.http.post(this.httpUrl+"/api/users/signup",user);
  }

}

export interface AuthToken {
  readonly token: string;
}
