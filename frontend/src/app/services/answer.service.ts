import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {FormModel} from "../models/form.model";
import {Observable} from "rxjs";
import {AnswerModel} from "../models/answer.model";
import {Statistics} from "../models/Statistics";
import {VarQuestionStatistics} from "../models/VarQuestionStatistics";
import {QuestionModel} from "../models/question.model";

@Injectable({
  providedIn: "root"
})
export class AnswerService {
  private httpUrl: string = "http://localhost:8081/api/answers";

  constructor(private http: HttpClient) {
  }

  public saveAnswers(answers: AnswerModel[]): Observable<any> {
    return this.http.post(this.httpUrl, answers);
  }

  public getStatisticOfAllVariantQuestionByFormId(id: string): Observable<VarQuestionStatistics[]> {
    return this.http.get<VarQuestionStatistics[]>(this.httpUrl + "/form/" + id + "/statistics");
  }
  public  getAllTextAnswersByQuestionId(id:string):Observable<AnswerModel[]>{
    return this.http.get<AnswerModel[]>(this.httpUrl+"/text/"+id);
  }


}
