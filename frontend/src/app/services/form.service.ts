import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Form} from "../models/form.model.home.view";
import {FormModel} from "../models/form.model";
import {FormStatistics} from "../models/FormStatistics";


@Injectable({
  providedIn: "root"
})

export class FormService {

  private httpUrl: string = "http://localhost:8081/api";

  constructor(private http: HttpClient) {
  }

  public saveForm(formModel: FormModel): Observable<any> {
    return this.http.post(this.httpUrl + "/forms", formModel);
  }

  public getAllForms(page: number): Observable<Form[]> {
    return this.http.get<Form[]>(this.httpUrl + "/forms/home/public?page=" + page);
  }

  public getAllPrivateForms(page: number): Observable<Form[]> {
    return this.http.get<Form[]>(this.httpUrl + "/forms/home/private?page=" + page);
  }

  public getFormById(id: string): Observable<FormModel> {
    return this.http.get<FormModel>(this.httpUrl + "/forms/" + id);
  }

  public getFormsByUserId(id: number): Observable<FormStatistics[]> {
    return this.http.get<FormStatistics[]>(this.httpUrl + "/forms/user/" + id)
  }

  public deleteForm(id: number): Observable<void> {
    return this.http.delete<void>(this.httpUrl + "/forms/" + id);
  }

  public getStatisticFormById(id: string): Observable<FormStatistics> {
    return this.http.get<FormStatistics>(this.httpUrl + "/forms/" + id + "/statistics");
  }

  public getPageCountPublic(): Observable<number> {
    return this.http.get<number>(this.httpUrl + "/forms/pagecount/public");
  }

  public getPageCountPrivate(): Observable<number> {
    return this.http.get<number>(this.httpUrl + "/forms/pagecount/private");
  }
}
