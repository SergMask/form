import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {QuestionModel} from "../models/question.model";
import {Form} from "../models/form.model.home.view";

@Injectable({
  providedIn: "root"
})

export class QuestionService {
  private httpUrl: string = "http://localhost:8081/api/questions";

  constructor(private http: HttpClient) {
  }

  public getAllQuestionsByFormId(id:string):Observable<QuestionModel[]>{
    return this.http.get<QuestionModel[]>(this.httpUrl+"/form/"+id)
  }
}
