import {QuestionModel} from "./question.model";

export class AnswerModel {
  id: number;
  answer: string;
  idVariant: number;
  question: QuestionModel;
}
