export class Form {
  id: number;
  name: string;
  isPrivate: string;
  username: string;


  constructor(id: number, name: string, isPrivate: string, username: string) {
    this.id = id;
    this.name = name;
    this.isPrivate = isPrivate;
    this.username = username;
  }
}
