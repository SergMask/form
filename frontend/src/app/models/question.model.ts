import {VariantModel} from "./variant.model";

export class QuestionModel {
  type: string;
  variants: VariantModel[];
  mandatory: string;
  name: string;
  id: string;
}
