import {RoleModel} from "./role.model";

export class UserModel {
  id: number;
  name:string;
  email:string;
  password:string;
  address:string;
  role:RoleModel;



}
