import {Statistics} from "./Statistics";

export class VarQuestionStatistics {
  private _id:number;
  private _statistics:Statistics[];




  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get statistics(): Statistics[] {
    return this._statistics;
  }

  set statistics(value: Statistics[]) {
    this._statistics = value;
  }
}
