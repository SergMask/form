export class FormStatistics{
  private _id:number;
  private _name:string;
  private _isPrivate:boolean;
  private _questionsCount:number;
  private _themeTagCount:number;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get isPrivate(): boolean {
    return this._isPrivate;
  }

  set isPrivate(value: boolean) {
    this._isPrivate = value;
  }

  get questionsCount(): number {
    return this._questionsCount;
  }

  set questionsCount(value: number) {
    this._questionsCount = value;
  }

  get themeTagCount(): number {
    return this._themeTagCount;
  }

  set themeTagCount(value: number) {
    this._themeTagCount = value;
  }

}
