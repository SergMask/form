export class Statistics {
  private _id:number;
  private _varName:string;
  private _percents:number;
  private _questionId:number;
  private _answerQuantity:number;


  get answerQuantity(): number {
    return this._answerQuantity;
  }

  set answerQuantity(value: number) {
    this._answerQuantity = value;
  }

  get questionId(): number {
    return this._questionId;
  }

  set questionId(value: number) {
    this._questionId = value;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get varName(): string {
    return this._varName;
  }

  set varName(value: string) {
    this._varName = value;
  }

  get percents(): number {
    return this._percents;
  }

  set percents(value: number) {
    this._percents = value;
  }

  constructor(id: number, varName: string, percents: number) {
    this._id = id;
    this._varName = varName;
    this._percents = percents;
  }
}
