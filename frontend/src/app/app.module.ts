import { BrowserModule } from "@angular/platform-browser";
import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { ModalModule } from "ngx-bootstrap/modal";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {Ng4LoadingSpinnerModule} from "ng4-loading-spinner";
import {RouterModule, Routes} from "@angular/router";
import {NotFoundComponent} from "./modules/layout/components/404/not-found.component";
import {LayoutModule} from "./modules/layout/layout.module";
import {HomeComponent} from "./modules/layout/components/home/home.component";
import {AnswerComponent} from "./modules/layout/components/answer/answer.component";
import {ProfilePageComponent} from "./modules/layout/components/profile-page/profile-page.component";
import {CreatePageComponent} from "./modules/layout/components/create-page/create-page.component";
import {SharedModule} from "./shared/shared.module";
import {BufferPageComponent} from "./modules/layout/components/buffer-page/buffer-page.component";
import {RegisterPageComponent} from "./modules/layout/components/register-page/register-page.component";
import {APIInterceptor} from "./interceptors/api-interceptors";
import {UserService} from "./services/user.service";
import {PrivateFormsComponent} from "./modules/layout/components/private-forms/private-forms.component";
import {ReactiveFormsModule} from "@angular/forms";
import {AnswerBufferPageComponent} from "./modules/layout/components/answer-buffer-page/answer-buffer-page.component";
import {StatisticsComponent} from "./modules/layout/components/statistics/statistics.component";


const appRoutes: Routes = [
  {path: "", component: HomeComponent},
  {path: "home", component: HomeComponent},
  {path: "profile", component: ProfilePageComponent},
  {path: "answer/:id", component: AnswerComponent},
  {path: "create-page", component: CreatePageComponent},
  {path:"buffer-page",component:BufferPageComponent},
  {path:"register-page",component:RegisterPageComponent},
  {path:"private-forms",component:PrivateFormsComponent},
  {path:"answer-buffer-page",component:AnswerBufferPageComponent},
  {path:"statistic/:id",component:StatisticsComponent},
  {path: "**", component: NotFoundComponent}


];

@NgModule({
  declarations: [
    AppComponent

     ],
  imports: [
    BrowserModule,
    FormsModule,
    LayoutModule,
    HttpClientModule,
    Ng4LoadingSpinnerModule.forRoot(),
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [UserService, APIInterceptor, {
    provide: HTTP_INTERCEPTORS,
    useClass: APIInterceptor,
    multi: true
  }],
  schemas: [NO_ERRORS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
