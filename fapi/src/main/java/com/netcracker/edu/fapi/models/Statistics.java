package com.netcracker.edu.fapi.models;

public class Statistics {
    private int id;
    private String varName;
    private double percents;
    private int questionId;
    private int answerQuantity;

    public int getAnswerQuantity() {
        return answerQuantity;
    }

    public void setAnswerQuantity(int answerQuantity) {
        this.answerQuantity = answerQuantity;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName = varName;
    }

    public double getPercents() {
        return percents;
    }

    public void setPercents(double percents) {
        this.percents = percents;
    }
}
