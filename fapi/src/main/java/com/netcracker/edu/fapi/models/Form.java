package com.netcracker.edu.fapi.models;

import java.util.List;
import java.util.Objects;


public class Form {


    private int id;
    private Boolean isPrivate;
    private String name;
    private User user;
    private List<Question> questions;
    private List<ThemeTag> themeTags;

    public List<ThemeTag> getThemeTags() {
        return themeTags;
    }

    public void setThemeTags(List<ThemeTag> themeTags) {
        this.themeTags = themeTags;
    }

    public List<Question> getQuestions() {
        return questions;
    }


    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Form() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Form form = (Form) o;
        return id == form.id &&
                user == form.user &&
                Objects.equals(isPrivate, form.isPrivate) &&
                Objects.equals(name, form.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isPrivate, name, user);
    }

    @Override
    public String toString() {
        return "Form{" +
                "id=" + id +
                ", isPrivate='" + isPrivate + '\'' +
                ", name='" + name + '\'' +
                ", idUser=" + user +
                '}';
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
