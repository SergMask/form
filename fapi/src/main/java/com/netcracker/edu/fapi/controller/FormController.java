package com.netcracker.edu.fapi.controller;

import com.netcracker.edu.fapi.converter.FormConverter;
import com.netcracker.edu.fapi.converter.FormStatisticsConverter;
import com.netcracker.edu.fapi.models.Form;
import com.netcracker.edu.fapi.models.FormStatistics;
import com.netcracker.edu.fapi.models.FormUI;
import com.netcracker.edu.fapi.service.FormService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@RestController
@RequestMapping("/api")
public class FormController {

    @Autowired
    private FormConverter formConverter;
    @Autowired
    private FormStatisticsConverter formStatisticsConverter;
    @Autowired
    private FormService formService;

    @RequestMapping(value = "/forms/home/public", method = RequestMethod.GET)
    public ResponseEntity<List<FormUI>> getAllPublicFormsHome(@RequestParam int page) {
        return ResponseEntity.ok(this.formConverter.convertFormsPublic(this.formService.getAllForms(page, 0)));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/forms/home/private", method = RequestMethod.GET)
    public ResponseEntity<List<FormUI>> getAllPrivateFormsHome(@RequestParam int page) {
        return ResponseEntity.ok(this.formConverter.convertFormsPrivate(this.formService.getAllForms(page, 1)));
    }

    @RequestMapping(value = "/forms", method = RequestMethod.POST)
    public ResponseEntity<Form> saveForm(@RequestBody Form form) {
        if (form != null) {
            return ResponseEntity.ok(formService.saveForm(form));
        }
        return ResponseEntity.badRequest().build();
    }


    @RequestMapping(value = "/forms/{id}", method = RequestMethod.GET)
    public ResponseEntity<Form> getFormById(@PathVariable(name = "id") Integer id) {
        Form form = new Form();
        form = this.formService.getFormById(id);
        if (!form.getPrivate()) {
            return ResponseEntity.ok(this.formService.getFormById(id));
        } else {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if ((authentication.getPrincipal()) instanceof User) {
                return ResponseEntity.ok(this.formService.getFormById(id));
            } else return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }


    }

    @RequestMapping(value = "/forms/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<FormStatistics>> getFormsByUserId(@PathVariable int id) {
        return ResponseEntity.ok(this.formStatisticsConverter.convertForms(this.formService.getFormsByUserId(id)));
    }

    @RequestMapping(value = "forms/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteForm(@PathVariable int id) {
        formService.deleteForm(id);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "forms/{id}/statistics", method = RequestMethod.GET)
    public ResponseEntity<FormStatistics> getStatisticFormById(@PathVariable int id) {
        return ResponseEntity.ok(this.formStatisticsConverter.convert(this.formService.getFormById(id)));
    }

    @RequestMapping(value = "forms/pagecount/public", method = RequestMethod.GET)
    public ResponseEntity<Integer> getCountPublic() {
        return ResponseEntity.ok((this.formService.getPageCount(false)));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "forms/pagecount/private", method = RequestMethod.GET)
    public ResponseEntity<Integer> getCountPrivate() {
        return ResponseEntity.ok((this.formService.getPageCount(true)));
    }

    @RequestMapping(value = "forms/themetag/{id}")
    public ResponseEntity<List<FormUI>> getFormsByThemeTagId(@PathVariable int id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if ((authentication.getPrincipal()) instanceof User) {
            return ResponseEntity.ok(this.formConverter.convertFormsAll(this.formService.getFormsByThemeTagId(id)));
        }
        else{
            return ResponseEntity.ok(this.formConverter.convertFormsPublic(this.formService.getFormsByThemeTagId(id)));
        }

    }


}
