package com.netcracker.edu.fapi.controller;

import com.netcracker.edu.fapi.models.Answer;
import com.netcracker.edu.fapi.models.Statistics;
import com.netcracker.edu.fapi.models.VarQuestionStatistics;
import com.netcracker.edu.fapi.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/answers")
public class AnswerController {
    @Autowired
    private AnswerService answerService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<List<Answer>> saveAnswers(@RequestBody List<Answer> answer) {
        if (answer != null) {
            return ResponseEntity.ok(answerService.saveAnswers(answer));
        }
        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(value = "/text/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Answer>> getAllTextAnswersByQuestionId(@PathVariable int id) {
        return ResponseEntity.ok(answerService.getAllTextAnswersByQuestionId(id));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/form/{id}/statistics", method = RequestMethod.GET)
    public ResponseEntity<List<VarQuestionStatistics>> getStatisticOfAllVariantQuestionByFormId(@PathVariable int id) {
        return ResponseEntity.ok(answerService.getStatisticOfAllVariantQuestionByFormId(id));
    }
}
