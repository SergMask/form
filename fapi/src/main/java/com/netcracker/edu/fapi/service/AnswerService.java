package com.netcracker.edu.fapi.service;

import com.netcracker.edu.fapi.models.Answer;
import com.netcracker.edu.fapi.models.Statistics;
import com.netcracker.edu.fapi.models.VarQuestionStatistics;

import java.util.List;

public interface AnswerService {
    List<Answer> saveAnswers(List<Answer> answer);
    List<Answer> getAllTextAnswersByQuestionId(int id);
    List<VarQuestionStatistics> getStatisticOfAllVariantQuestionByFormId(int id);

}
