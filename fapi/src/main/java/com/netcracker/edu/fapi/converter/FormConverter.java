package com.netcracker.edu.fapi.converter;

import com.netcracker.edu.fapi.models.Form;
import com.netcracker.edu.fapi.models.FormUI;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FormConverter {

    private FormUI convertForm(Form form) {
        FormUI formUI = new FormUI();
        formUI.setId(form.getId());
        formUI.setPrivate(form.getPrivate());
        formUI.setName(form.getName());
        formUI.setUsername(form.getUser().getName());
        return formUI;
    }

    public List<FormUI> convertFormsPublic(List<Form> forms) {
        return forms.stream().filter(form -> !form.getPrivate()).map(this::convertForm).collect(Collectors.toList());
    }
    public List<FormUI> convertFormsPrivate(List<Form> forms) {
        return forms.stream().filter(Form::getPrivate).map(this::convertForm).collect(Collectors.toList());
    }
    public List<FormUI> convertFormsAll(List<Form> forms) {
        return forms.stream().map(this::convertForm).collect(Collectors.toList());
    }
}
