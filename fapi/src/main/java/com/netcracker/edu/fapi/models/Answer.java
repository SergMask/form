package com.netcracker.edu.fapi.models;

import java.util.Objects;

public class Answer {
    private int id;
    private String answer;
    private Integer idVariant;
    private Question question;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getIdVariant() {
        return idVariant;
    }

    public void setIdVariant(Integer idVariant) {
        this.idVariant = idVariant;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer1 = (Answer) o;
        return id == answer1.id &&
                Objects.equals(answer, answer1.answer) &&
                Objects.equals(idVariant, answer1.idVariant) &&
                Objects.equals(question, answer1.question);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, answer, idVariant, question);
    }
}
