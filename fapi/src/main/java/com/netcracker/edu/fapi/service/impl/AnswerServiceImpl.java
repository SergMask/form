package com.netcracker.edu.fapi.service.impl;

import com.netcracker.edu.fapi.models.Answer;
import com.netcracker.edu.fapi.models.Statistics;
import com.netcracker.edu.fapi.models.VarQuestionStatistics;
import com.netcracker.edu.fapi.service.AnswerService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class AnswerServiceImpl implements AnswerService {
    @Value("${backend.server.url}")
    private String backendServerURL;


    @Override
    public List<Answer> saveAnswers(List<Answer> answer) {
        RestTemplate restTemplate = new RestTemplate();
        Answer[] answers = restTemplate.postForObject(backendServerURL + "api/answers/list", answer, Answer[].class);
        return Arrays.asList(answers);
    }

    @Override
    public List<Answer> getAllTextAnswersByQuestionId(int id) {
        RestTemplate restTemplate = new RestTemplate();
        Answer[] answers = restTemplate.getForObject(backendServerURL + "/api/answers/text/" + id, Answer[].class);
        return answers == null ? Collections.emptyList() : Arrays.asList(answers);
    }

    @Override
    public List<VarQuestionStatistics> getStatisticOfAllVariantQuestionByFormId(int id) {
        RestTemplate restTemplate = new RestTemplate();
        VarQuestionStatistics[] statistics = restTemplate.getForObject(backendServerURL + "/api/answers/form/" + id + "/statistics", VarQuestionStatistics[].class);
        return statistics == null ? Collections.emptyList() : Arrays.asList(statistics);
    }
}

