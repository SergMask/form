package com.netcracker.edu.fapi.models;

import java.util.Objects;

public class FormUI {
    private int id;
    private Boolean isPrivate;
    private String name;
    private String username;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FormUI formUI = (FormUI) o;
        return id == formUI.id &&
                Objects.equals(isPrivate, formUI.isPrivate) &&
                Objects.equals(name, formUI.name) &&
                Objects.equals(username, formUI.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isPrivate, name, username);
    }
}
