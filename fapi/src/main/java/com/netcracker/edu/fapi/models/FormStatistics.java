package com.netcracker.edu.fapi.models;

public class FormStatistics {
    private int  id;
    private String name;
    private boolean isPrivate;
    private int questionsCount;
    private  int themeTagCount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public int getQuestionsCount() {
        return questionsCount;
    }

    public void setQuestionsCount(int questionsCount) {
        this.questionsCount = questionsCount;
    }

    public int getThemeTagCount() {
        return themeTagCount;
    }

    public void setThemeTagCount(int themeTagCount) {
        this.themeTagCount = themeTagCount;
    }
}
