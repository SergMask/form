package com.netcracker.edu.fapi.service.impl;

import com.netcracker.edu.fapi.models.Form;
import com.netcracker.edu.fapi.models.User;
import com.netcracker.edu.fapi.service.FormService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class FormServiceImpl implements FormService {

    @Value("${backend.server.url}")
    private String backendServerURL;

    @Override
    public List<Form> getAllForms(int page, int route) {

        RestTemplate restTemplate = new RestTemplate();
        if (route == 0) {
            Form[] formsResponse = restTemplate.getForObject(backendServerURL + "/api/forms/public?page=" + page, Form[].class);
            return formsResponse == null ? Collections.emptyList() : Arrays.asList(formsResponse);
        }
        if (route == 1) {
            Form[] formsResponse = restTemplate.getForObject(backendServerURL + "/api/forms/private?page=" + page, Form[].class);
            return formsResponse == null ? Collections.emptyList() : Arrays.asList(formsResponse);
        } else {
            Form[] formsResponse = restTemplate.getForObject(backendServerURL + "/api/forms?page=" + page, Form[].class);
            return formsResponse == null ? Collections.emptyList() : Arrays.asList(formsResponse);
        }
    }

    @Override
    public Form saveForm(Form form) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(backendServerURL + "api/forms", form, Form.class).getBody();
    }

    @Override
    public Form getFormById(Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(backendServerURL + "/api/forms/" + id, Form.class);
    }

    @Override
    public List<Form> getFormsByUserId(int id) {
        RestTemplate restTemplate = new RestTemplate();
        Form[] formResponse = restTemplate.getForObject(backendServerURL + "/api/forms/user/" + id, Form[].class);
        return formResponse == null ? Collections.emptyList() : Arrays.asList(formResponse);
    }

    @Override
    public void deleteForm(int id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(backendServerURL + "/api/forms/" + id);
    }

    @Override
    public Integer getPageCount(Boolean isPrivate) {
        RestTemplate restTemplate = new RestTemplate();
        if(isPrivate){
            return restTemplate.getForObject(backendServerURL + "/api/forms/pagecount/private", int.class);
        }
        else {
            return restTemplate.getForObject(backendServerURL + "/api/forms/pagecount/public", int.class);
        }


    }

    @Override
    public List<Form> getFormsByThemeTagId(int id) {
        RestTemplate restTemplate= new RestTemplate();
        Form[] formResponse=restTemplate.getForObject(backendServerURL+"/api/forms/themetag/"+id,Form[].class);
        return formResponse==null?Collections.emptyList():Arrays.asList(formResponse);
    }
}
