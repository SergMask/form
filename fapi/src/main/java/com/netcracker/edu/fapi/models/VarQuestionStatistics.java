package com.netcracker.edu.fapi.models;

import java.util.List;

public class VarQuestionStatistics {
    int id;

    List<Statistics> statistics;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Statistics> getStatistics() {
        return statistics;
    }

    public void setStatistics(List<Statistics> statistics) {
        this.statistics = statistics;
    }
}
