package com.netcracker.edu.fapi.controller;

import com.netcracker.edu.fapi.models.Question;
import com.netcracker.edu.fapi.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/questions")
public class QuestionController {
    @Autowired
    private QuestionService questionService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/form/{id}",method = RequestMethod.GET)
    public ResponseEntity<List<Question>> getAllQuestionsByFormId(@PathVariable int id){
        return ResponseEntity.ok(questionService.getAllQuestionsByFormId(id));
    }
}
