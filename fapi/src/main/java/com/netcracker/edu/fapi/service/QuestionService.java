package com.netcracker.edu.fapi.service;

import com.netcracker.edu.fapi.models.Question;

import java.util.List;

public interface QuestionService {
    List<Question> getAllQuestionsByFormId(int id);
}
