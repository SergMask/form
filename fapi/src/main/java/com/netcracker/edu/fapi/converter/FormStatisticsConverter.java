package com.netcracker.edu.fapi.converter;

import com.netcracker.edu.fapi.models.Form;
import com.netcracker.edu.fapi.models.FormStatistics;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FormStatisticsConverter  {

    public FormStatistics convert(Form source) {
        FormStatistics formStatistics=new FormStatistics();
        formStatistics.setId(source.getId());
        formStatistics.setName(source.getName());
        formStatistics.setPrivate(source.getPrivate());
        formStatistics.setQuestionsCount(source.getQuestions().size());
        formStatistics.setThemeTagCount(source.getThemeTags().size());
        return formStatistics;
    }
    public List<FormStatistics> convertForms(List<Form> forms) {
        return forms.stream().map(this::convert).collect(Collectors.toList());
    }
}
