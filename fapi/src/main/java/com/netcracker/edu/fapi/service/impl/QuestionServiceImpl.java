package com.netcracker.edu.fapi.service.impl;

import com.netcracker.edu.fapi.models.Question;
import com.netcracker.edu.fapi.service.QuestionService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
    @Value("${backend.server.url}")
    private String backendServerURL;

    @Override
    public List<Question> getAllQuestionsByFormId(int id) {
        RestTemplate restTemplate=new RestTemplate();
        Question[] questions=restTemplate.getForObject(backendServerURL+"/api/questions/form/"+id,Question[].class);
        return questions==null? Collections.emptyList(): Arrays.asList(questions);
    }


}
