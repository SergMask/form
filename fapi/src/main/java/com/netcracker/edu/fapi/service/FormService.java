package com.netcracker.edu.fapi.service;


import com.netcracker.edu.fapi.models.Form;
import com.netcracker.edu.fapi.models.FormUI;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface FormService {
    List<Form> getAllForms(int page,int route);
    Form saveForm(Form form);
    Form getFormById(Integer id);
    List<Form> getFormsByUserId(int id);
    void deleteForm(int id);
    Integer getPageCount(Boolean isPrivate);
    List<Form>  getFormsByThemeTagId(int id);
}
